function inicializar()
    {
        let lienzo = document.getElementById('miLienzo');
        let ctx = lienzo. getContext ('2d');
        let vo = document.getElementById("Vo");
        let wo = document.getElementById("Wo");
        let radianes = Math.PI*wo.value/180;
        let tiempo=0;
       //Rotación de una figura a partir de su centro
        //matriz de rotacion
        ctx.translate(38, 220);
        ctx.rotate(radianes);
        ctx.translate(-38, -220);
        //rotado
        ctx.fillStyle = "orange";
        ctx.fillRect(55, 215, 30, 110);
        for(i=1;i<600;i++)//ciclo para los circulos que describen la trayectoria en función del tiempo
        {
            let distancia= vo.value* Math.cos(radianes)*tiempo;
            let altura = vo.value * Math.sin(radianes)* tiempo- 0.5*9.8*Math.pow(tiempo,2);
            ctx.beginPath();
            ctx.fillStyle="purple";
            ctx.arc(65+distancia, 225-altura, 7, 0, Math.PI * 2, false);
            ctx.stroke();
             ctx.fill();
             tiempo+=1;
        }

      

    }

